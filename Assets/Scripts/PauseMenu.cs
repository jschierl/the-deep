﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public static bool GameIsOver = false;
    public GameObject pauseMenu;
    public GameObject endMenu;

    void Start() {
        GameObject.Find("PauseMenu").SetActive(false);
        GameObject.Find("EndMenu").SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Diver.health <= 0) {
            endMenu.SetActive(true);
            GameIsOver = true;
        }
        else if (Input.GetKeyDown(KeyCode.Escape)) {
            if (GameIsPaused) {
                Resume();
            } else {
                Pause();
            }
        }


    }

    public void Resume() {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    public void Pause() {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void MainMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    public void Quit() {
        Application.Quit();
    }
}
