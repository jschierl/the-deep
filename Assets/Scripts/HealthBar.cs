﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    Image imageComponent;
    public Sprite full;
    public Sprite eat1;
    public Sprite eat2;
    public Sprite eat3;
    public Sprite eat4;
    public Sprite eat5;
    public Sprite dead;

    // Start is called before the first frame update
    void Start()
    {
        //Our image component is the one attached to this gameObject
        imageComponent = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Diver.health >= 100) {
            imageComponent.sprite = full;
        }
        else if ((Diver.health < 100) && (Diver.health >= 85)) {
            imageComponent.sprite = eat1;
        }
        else if ((Diver.health < 85) && (Diver.health >= 65)) {
            imageComponent.sprite = eat2;
        }
        else if ((Diver.health < 65) && (Diver.health >= 45)) {
            imageComponent.sprite = eat3;
        }
        else if ((Diver.health < 45) && (Diver.health >= 25)) {
            imageComponent.sprite = eat4;
        }
        else if ((Diver.health < 25) && (Diver.health > 0)) {
            imageComponent.sprite = eat5;
        }
        else if (Diver.health <= 0) {
            imageComponent.sprite = dead;
        }
    }
}
