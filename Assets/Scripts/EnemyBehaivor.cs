﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaivor : MonoBehaviour {
    public int health;
    public int speed = 1;
    public float sight = 1.0f;
    
    public bool movementCrab;
    public bool movementBlooper;
    public bool movementFloater;
    public bool movementLamprey;
    public bool movementInverse;


    public bool attackCrab;
    public bool attackBlooper;
    public bool attackFloater;
    public bool attackLamprey;
    public bool attackUrchin;
    public bool attackBun;
    public bool attackBobbit;

    public GameObject projectile;
    int movementTimer = 0;
    Vector2 movementTarget;
    bool moving = false;
    Vector2 direction;
    bool attacking = false;
    int attackTimer = 0;
    GameObject player;
    float aggroradius = 1f;
    int attackstage = 0;
    Vector2 startinglocation;
	// Use this for initialization
	void Start () {
        startinglocation = this.transform.position;
        player = GameObject.FindWithTag("Player");
        Rigidbody2D rigid = GetComponent<Rigidbody2D>();
        if (movementCrab) {
            rigid.gravityScale = 1;
            moving = true;
        }
        if (movementBlooper)
        {
            rigid.gravityScale = 1f;
        }
        if (movementInverse)
        {
            direction = Vector2.right;
        }
        else
        {
            direction = Vector2.left;
        }
        if (movementLamprey)
        {
            
            moving = true;
        }
        aggroradius = aggroradius * sight;
    }

    // Update is called once per frame
    void Update() {
        Rigidbody2D rigid = GetComponent<Rigidbody2D>();
        RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, direction, 1f);
        Vector2 raycasttarget = player.transform.position-transform.position;
        RaycastHit2D[] playerscan = Physics2D.RaycastAll(transform.position, raycasttarget, aggroradius);
        //Generic Timer Ticks
        if (!moving && !attacking)
        {
            movementTimer++;
            
        }
        if (attacking)
        {
            attackTimer++;
        } 
        if (moving && attackBlooper && !movementBlooper)
        {
            movementTimer++;
            
        }
        //Blooper and Floater Movement and Blooper attack
        if ((movementBlooper||movementFloater||attackBlooper) && movementTimer > 240 && (!moving || (attackBlooper)))
        {
            //scan for player. If no player, blorp in a random direction upwards
            if (playerscan.Length != 1 && (attackBlooper || attackFloater))
            {
                for (int i = 0; i < playerscan.Length-1; i++)
                {
                    if (playerscan[i].collider.tag == "Player")
                    {
                        movementTarget = player.transform.position;
                        if (movementBlooper || movementFloater)
                        {
                            moving = true;
                        }

                        if (attackFloater || attackBlooper)
                        {
                            attacking = true;

                        }
                    }
                }
                    
                
            }
            if ((movementBlooper || movementFloater) && !attacking && !moving)
            {
                Vector2 RandomCircle = Random.insideUnitCircle;
                if (movementBlooper && RandomCircle.y < 1)
                {
                    RandomCircle.y = 2;
                }
                
                movementTarget.x = transform.position.x + RandomCircle.x;
                movementTarget.y = transform.position.y + RandomCircle.y;
                moving = true;
                if (transform.position.x < startinglocation.x - 10)
                {
                    movementTarget.x = startinglocation.x;
                }
                if (transform.position.y < startinglocation.y - 10)
                {
                    movementTarget.y = startinglocation.y;
                }
                if (transform.position.x > startinglocation.x + 10)
                {
                    movementTarget.x = startinglocation.x;
                }
                if (transform.position.y > startinglocation.y + 10)
                {
                    movementTarget.y = startinglocation.y;
                }
            }
                
                movementTimer = 0;
                if (movementBlooper || attackBlooper)
                {
                    rigid.gravityScale = 0.0f;
                }
                rigid.velocity = Vector2.zero;
            
            
            
        }
        if (((movementBlooper || movementFloater) && moving) || (attackBlooper && attacking))
        {
            
            if (movementBlooper || attackBlooper)
            {
                transform.position = Vector2.MoveTowards(gameObject.transform.position, movementTarget, 0.05f * speed);
                if (gameObject.transform.position.Equals(movementTarget))
                {
                    if (movementBlooper)
                    {
                        moving = false;
                    }
                    if (attackBlooper)
                    {
                        attacking = false;
                        rigid.gravityScale = 1f;
                    }
                    if (movementBlooper)
                    {
                        rigid.gravityScale = 0.4f;
                    }

                }
            }
            if (movementFloater)
            {
                transform.position = Vector2.MoveTowards(gameObject.transform.position, movementTarget, -0.05f * speed);
                movementTimer++;
                if (movementTimer == 30)
                {
                    moving = false;
                    movementTimer = 0;
                }
                                 
                
            }
            
        }
        //Crab Movement
        if (movementCrab && moving)
        { 
                transform.Translate(direction * Time.deltaTime * 2 * speed);
        }
        if (movementCrab && moving)
        {
            if (hit.Length != 1)
            {

                if (hit[1].collider.tag == "Terrain")
                {
                    direction = direction * -1;
                }
            }

        }
        //Crab Attack
  /*      if (attackCrab && moving)
        {
            if (hit.Length != 1)
            {
                if (hit[1].collider.tag == "Player" || collider.gameObject.tag == "diver")
                {
                    
                    attacking = true;
                    moving = false;
                    //Insert Changing animation to attack animation
                }
            }
        }
        if (attackCrab && attacking && attackTimer == 40)
        {
            if (hit.Length != 1)
            {
                if (hit[1].collider.tag == "Player" || collider.gameObject.tag == "diver")
                {
                    player.SendMessage("takeDamage");
                }
            }
            attackTimer = -40;
        } */
        if (attackCrab && attacking && attackTimer == -1)
        {
            moving = true;
            attacking = false;
            attackTimer = 0;
        }
        //Floater Attack
        if (attackFloater && attacking)
        {
            attacking = false;
            GameObject fireball = Instantiate(projectile, this.transform);
        }
        //Lamprey Move
        if (movementLamprey && moving)
        {
            if (playerscan.Length != 1)
            {
                for (int i = 0; i < playerscan.Length - 1; i++)
                {
                    if (playerscan[i].collider.tag == "Player")
                    {
                        movementTarget = player.transform.position;
                        transform.position = Vector2.MoveTowards(gameObject.transform.position, movementTarget, 0.005f * speed);
                    }
                }
            }
        }
        //Lamprey Attack
        if (attackLamprey && attacking && (attackTimer > 100))
        {
            attackTimer = 0;
            player.SendMessage("takeDamage");
        }
        //Bobbit attack part 2
        if (attackBobbit && attacking && player.transform.position.Equals(gameObject.transform.position) && attackstage == 0)
        {
            Debug.Log("bbb");
            Vector3 MoveDownwards = gameObject.transform.position;
            MoveDownwards.y = MoveDownwards.y - 2;
            player.SendMessage("setTarget", MoveDownwards);
            attackstage = 1;
            movementTarget = MoveDownwards;
        }
        if (attackBobbit && attacking && attackstage == 1)
        {
            transform.position = Vector2.MoveTowards(gameObject.transform.position, movementTarget, 0.05f * speed);
            if (gameObject.transform.position.Equals(movementTarget))
            {
                attackstage = 2;
                attackTimer = 0;
            }
        }
        if (attackBobbit && attacking && (attackTimer > 100) && attackstage == 2)
        {
            attackTimer = 0;
            player.SendMessage("takeDamage");
        }
        
    }
    void OnCollisionEnter2D(Collision2D collider)
    {
        
        //Lamprey Grab
        if (attackLamprey)
        {
            
            if (collider.gameObject.tag == "Player" || collider.gameObject.tag == "diver")
            {
                
                player.SendMessage("setMoving", false);
                moving = false;
                attacking = true;
            }
            
        }
        if (attackUrchin)
        {
            if (collider.gameObject.tag == "Player" || collider.gameObject.tag == "diver")
            {
                player.SendMessage("takeDamage");
            }
            
        }
        if (attackBun)
        {
            if (collider.gameObject.tag == "Player" || collider.gameObject.tag == "diver")
            {
                player.SendMessage("slow");
                destroySelf();
            }
            

        }
        if (attackBobbit)
        {
            if (collider.gameObject.tag == "Player" || collider.gameObject.tag == "diver")
            {
                Rigidbody2D rigid = GetComponent<Rigidbody2D>();
                rigid.velocity = Vector2.zero;
                //disable player movement
                player.SendMessage("setMoving", false);
                //move player downwards at the same pace as bobbit into the terrain

                player.SendMessage("setTarget", gameObject.transform.position);
                player.SendMessage("toggleCollider");
                toggleCollider();
                attacking = true;
            }
            

        }
        if (movementBlooper && moving)
        {
            movementTarget = transform.position;
        }
    }
    void onDamageTaken()
    {
        health = health - 1;
        if (health < 1)
        {
            destroySelf();
            
        }
    }
    void toggleCollider()
    {
        Collider2D Col = GetComponent<Collider2D>();
        Col.enabled = !Col.enabled;
    }
    
    void destroySelf()
    {
        if (attackBobbit && attacking && attackstage != 0)
        {
            player.SendMessage("onScam");
            Vector3 moveback = gameObject.transform.position;
            moveback.y = moveback.y + 2;
            player.SendMessage("setTarget", moveback);
        }
        Destroy(gameObject);
    }
}
