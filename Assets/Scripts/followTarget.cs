﻿using System.Collections;
using UnityEngine;

public class followTarget : MonoBehaviour {

    //what we are folloiwng
    public Transform target;

    //zeroes out the velocity
    Vector3 velocity = Vector3.zero;

    //time to follow follow target
    public float smoothTime = .15f;

    //enable and set max Y value
    public bool YMaxEnabled = false;
    public float YMaxValue = 0;

    //enable and set min Y value
    public bool YMinEnabled = false;
    public float YMinValue = 0;

    //enable and set max X value
    public bool XMaxEnabled = false;
    public float XMaxValue = 0;

    //enable and set min X value
    public bool XMinEnabled = false;
    public float XMinValue = 0;

    void Update() {
        //target position
        Vector3 targetPos = target.position;

        //vertical clamping
        if (YMinEnabled && YMaxEnabled) {
            targetPos.y = Mathf.Clamp(target.position.y, YMinValue, YMaxValue);
        } else if (YMinEnabled) {
            targetPos.y = Mathf.Clamp(target.position.y, YMinValue, target.position.y);
        } else if (YMaxEnabled) {
            targetPos.y = Mathf.Clamp(target.position.y, target.position.y, YMaxValue);
        }

        //horizontal clamping
        if (XMinEnabled && XMaxEnabled) {
            targetPos.x = Mathf.Clamp(target.position.x, XMinValue, XMaxValue);
        } else if (XMinEnabled) {
            targetPos.x = Mathf.Clamp(target.position.x, XMinValue, target.position.y);
        } else if (XMaxEnabled) {
            targetPos.x = Mathf.Clamp(target.position.x, target.position.y, XMaxValue);
        }

        //align the camera and the targets z position
        targetPos.z = transform.position.z;

        //using SmoothDamp will gradually change camera catching up to target
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);
    }
}
