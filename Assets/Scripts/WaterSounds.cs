﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSounds : MonoBehaviour
{
    public AudioSource bgWater;
    public bool playing;

    // Start is called before the first frame update
    void Start()
    {
        playing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Diver.underwater) {
            if (! playing) {
                bgWater.Play();
                playing = true;
            }
        } else {
            playing = false;
            bgWater.Stop();
        }
    }
}
