using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

	public Dialogue dialogue;
    public DialogueSwitch publisher;
    public bool triggered;


    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            publisher.showButton();
            publisher.setDialogue(dialogue);
            triggered = true;
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            publisher.hideButton();
            triggered = false;
        }
    }
}
