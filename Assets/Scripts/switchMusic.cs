using UnityEngine;
using System.Collections;

public class switchMusic : MonoBehaviour
{

    public AudioSource _AudioSource;

    public AudioClip _AudioClip1;
    public AudioClip _AudioClip2;

    void Start()
    {
        _AudioSource.clip = _AudioClip1;
        _AudioSource.Play();

    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            if (_AudioSource.clip == _AudioClip1) {
                _AudioSource.clip = _AudioClip2;
                _AudioSource.Play();
            }
        }
    }

    void OnTriggerExit2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            if (_AudioSource.clip == _AudioClip2) {
                _AudioSource.clip = _AudioClip1;
                _AudioSource.Play();
            }
        }
    }
}
