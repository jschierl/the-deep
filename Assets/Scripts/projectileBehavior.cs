﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectileBehavior : MonoBehaviour {
    GameObject player;
    Vector2 raycasttarget;
    // Use this for initialization
    void Start () {
        transform.parent = null;
        player = GameObject.FindWithTag("Player");
        raycasttarget = player.transform.position - transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(raycasttarget * Time.deltaTime * 1);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            //Insert damaging the player
            Debug.Log("Player hit!");
            Destroy(gameObject);
        }
        if (other.tag == "Terrain")
        {
            Destroy(gameObject);
        }
    }
}
