using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveController : MonoBehaviour {

    Rigidbody2D rb;
    //GameManager gm;
    AudioSource audio;
    public Animator animator;

    //sound on flipping in water
    public AudioClip splash;
    //sound on hitting wall in water
    public AudioClip thud;
    //what sprite we are changing gravity for
    public Transform target;

    public int speed;
    public int jump;
    public int dash;
    public float speedMultiplier;
    public int speedmax;
    //change surface threshhold
    public float surface;
    //if underwater
    public bool underwater;
    //when to flip sprite animation
    public bool facingRight;
    public bool diverAttack;

    //David's stuff, will document later
    bool debuff = false;
    bool slowed = false;
    bool moving = true;
    bool scam = false;
    int debufftimer = 0;
    Vector2 movetarget;
    bool movingtotarget = false;
    public GameObject childcollider;
    public bool attacking = false;
    public int attacktimer = 0;

    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
        //gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        underwater = false;
        diverAttack = false;
    }

    void Update() {
        //only walk/dash when moving if above water
        animator.SetFloat("Speed", Mathf.Abs(rb.velocity.x)+Mathf.Abs(rb.velocity.y));
        animator.SetBool("Underwater", underwater);
        if (Input.GetKeyDown(KeyCode.F)) {
            //dash animation
            animator.SetFloat("Speed", 11);
        }
        if (Input.GetMouseButton(0)) {
            //player attack left-mouse click
            if (! diverAttack) {
                diverAttack = true;
            } else {
                diverAttack = false;
            }
            animator.SetBool("Attack", diverAttack);
        }
    }

    // Update is called once per frame
    void FixedUpdate () {

        //target position
        Vector3 targetPos = target.position;

        //vertical clamping
        if (targetPos.y > surface) {
            underwater = false;
            rb.gravityScale = 1.5f;
        } else if (targetPos.y < surface) {
            underwater = true;
            if (moving)
            {
                rb.gravityScale = 0.05f;
            }

        }

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, speed);

        // no more input if game is over OR if movement is turned off
        if (! PauseMenu.GameIsOver) {
            if (Input.GetKeyDown(KeyCode.Z) && !attacking)
            {

                Vector2 direction;
                attacking = true;

                if (facingRight)
                {

                    direction = Vector2.right;
                }
                else
                {
                    direction = Vector2.left;
                }
                //INSERT CHANGE SPRITE TO ATTACKING SPRITE AND ADD A SOUND EFFECT MAYBE
                RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, direction, 5f);
                for (int i = 0; i < hit.Length; i++)
                {
                    Debug.Log(hit[i].collider.tag);
                    if (hit[i].collider.tag == "Enemy")
                    {
                        hit[i].collider.gameObject.SendMessage("onDamageTaken");
                    }
                }
            }
            //Move timers up
            if (attacking)
            {
                //attacking cooldown
                attacktimer++;
                Debug.Log(attacktimer);
                if (attacktimer == 60)
                {
                    attacking = false;
                    attacktimer = 0;
                    //INSERT CHANGING SPRITE BACK TO NORMAL
                }
            }
            if (debuff)
            {
                debufftimer++;
                if (debufftimer == 300)
                {
                    debuff = false;
                    debufftimer = 0;
                    if (slowed)
                    {
                        slowed = false;
                        speed = speedmax;
                    }
                }
            }
            //If being forced to move, otherwise continue as normal
            if (movingtotarget)
            {
                Rigidbody2D rigid = GetComponent<Rigidbody2D>();

                transform.position = Vector2.MoveTowards(gameObject.transform.position, movetarget, 0.05f * 2);
                rigid.velocity = Vector2.zero;
                if (gameObject.transform.position.Equals(movetarget))
                {
                    rigid.gravityScale = 0;
                    movingtotarget = false;
                    if (scam)
                    {

                            rigid.gravityScale = 1;
                            setMoving(true);
                        toggleCollider();
                    }
                }
            }

            //All the stuff Jack put in for movement
                else if (moving)
                {
                    //UP
                    if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)))
                    {
                        //only if swimming
                        if (underwater)
                        {
                            rb.AddForce(transform.up * speed);
                        }
                    }

                    //DOWN
                    if (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)))
                    {
                        if (underwater)
                        {
                            rb.AddForce(transform.up * speed * -1);
                        }
                        else
                        {
                            rb.AddForce(transform.up * speed * speedMultiplier * -1);
                        }
                    }

                    //LEFT
                    if (Input.GetKey(KeyCode.A) || (Input.GetKey(KeyCode.LeftArrow)))
                    {
                        if (underwater)
                        {
                            rb.AddForce(transform.right * speed);
                        }
                        else
                        {
                            rb.AddForce(transform.right * speed * speedMultiplier);
                        }
                    }

                    //RIGHT
                    if (Input.GetKey(KeyCode.D) || (Input.GetKey(KeyCode.RightArrow)))
                    {
                        if (underwater)
                        {
                            rb.AddForce(transform.right * speed * -1);
                        }
                        else
                        {
                            rb.AddForce(transform.right * speed * speedMultiplier * -1);
                        }
                    }

                    //JUMP
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        //if underwater shoot harpoon
                        if (underwater)
                        {


                        }
                        //if abovewater jump
                        else
                        {
                            rb.AddForce(transform.up * jump);
                            //rb.AddForce(transform.TransformDirection(Vector3.up) * jump);
                            //rb.velocity += jump * Vector2.up;
                            //transform.position += (Vector3.up * jump * Time.deltaTime);
                        }
                    }

                    //DASH left-right
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        if (underwater)
                        {
                            if (facingRight)
                            {
                                rb.AddForce(transform.right * dash * -1);
                            }
                            else
                            {
                                rb.AddForce(transform.right * dash);
                            }
                        }
                    }

                    //DASH up
                    if (Input.GetKeyDown(KeyCode.F) && (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow))))
                    {
                        if (underwater)
                        {
                            rb.AddForce(transform.up * dash);
                        }
                    }

                    //DASH down
                    if (Input.GetKeyDown(KeyCode.F) && (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow))))
                    {
                        if (underwater)
                        {
                            rb.AddForce(transform.up * dash * -1);
                        }
                    }

            }


            Flip(Input.GetAxis("Horizontal"));

        }

    }

    void OnCollisionEnter2D(Collision2D col) {
        if (underwater) {
            audio.clip = thud;
            audio.Play();
        }
    }

    private void Flip(float horizontal) {
        if ((horizontal > 0 && !facingRight) || (horizontal < 0 && facingRight)) {
            facingRight = !facingRight;
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;

            if (underwater) {
                audio.clip = splash;
                audio.Play();
            }
        }
    }
    //More enemy interaction stuff
    void setMoving(bool yesno)
    {
        moving = yesno;
    }

    void setTarget(Vector3 target)
    {
        movingtotarget = true;
        Rigidbody2D rigid = GetComponent<Rigidbody2D>();
        rigid.gravityScale = 0;
        rigid.velocity = Vector2.zero;
        movetarget = target;
    }
    void toggleCollider()
    {
        Collider2D Col = childcollider.GetComponent<Collider2D>();
        Col.enabled = !Col.enabled;
    }
    void onScam()
    {
        scam = true;
    }
    void slow()
    {
        speed = (speedmax / 2);
        debuff = true;
        slowed = true;
    }
    void takeDamage()
    {
        //INSERT HEALTH DAMAGE SCRIPT
    }
}
