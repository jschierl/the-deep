﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {
    bool moving = true;
    bool scam = false;
    Vector2 movetarget;
    bool movingtotarget = false;
    int debufftimer = 0;
    int speed = 10;
    bool debuff = false;
    bool slowed = false;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (debuff)
        {
            debufftimer++;
            if (debufftimer == 300)
            {
                debuff = false;
                debufftimer = 0;
                if (slowed)
                {
                    slowed = false;
                    speed = 10;
                }
            }
        }
        if (movingtotarget)
        {
            transform.position = Vector2.MoveTowards(gameObject.transform.position, movetarget, 0.05f * 2);
            if (gameObject.transform.position.Equals(movetarget))
            {
                Debug.Log("ccc");
                movingtotarget = false;
                if (scam)
                {
                    setMoving(true);
                    toggleCollider();
                }
            }
        }
        else if (moving)
        {
            if (Input.GetKey(KeyCode.D))
            {

                transform.Translate(-Vector3.left * Time.deltaTime * speed);


            }
            if (Input.GetKey(KeyCode.W))
            {

                transform.Translate(Vector3.up * Time.deltaTime * speed);

            }
            if (Input.GetKey(KeyCode.S))
            {

                transform.Translate(-Vector3.up * Time.deltaTime * speed);

            }
            if (Input.GetKey(KeyCode.A))
            {

                transform.Translate(Vector3.left * Time.deltaTime * speed);

            }
        }
    }
    void setMoving(bool yesno)
    {
        moving = yesno;
    }
    
    void setTarget(Vector3 target)
    {
        movingtotarget = true;
        movetarget = target;
    }
    void toggleCollider()
    {
        Collider2D Col = GetComponent<Collider2D>();
        Col.enabled = !Col.enabled;
    }
    void onScam()
    {
        scam = true;
    }
    void slow()
    {
        speed = 4;
        debuff = true;
        slowed = true;
    }
    void takeDamage()
    {
        //INSERT HEALTH DAMAGE SCRIPT
    }

}
