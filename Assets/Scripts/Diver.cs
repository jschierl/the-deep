﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diver : MonoBehaviour
{
    public static int health = 100;
    public static bool underwater;
    public moveController controller;

    // Start is called before the first frame update
    void Start()
    {
        underwater = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)) {
            health = health - 5;
        }

        underwater = controller.underwater;
    }
}
