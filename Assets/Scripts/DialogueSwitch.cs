using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueSwitch : MonoBehaviour {

    public Dialogue dialogue;
    public GameObject source;

    void Start() {
        source.SetActive(false);
    }

    public void showButton() {
        source.SetActive(true);
    }

    public void hideButton() {
        source.SetActive(false);
    }

    public void setDialogue(Dialogue newDialogue) {
        dialogue = newDialogue;
    }

    public void TriggerDialogue() {
        hideButton();
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }
}
