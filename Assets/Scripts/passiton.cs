﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class passiton : MonoBehaviour
{
    public GameObject diver;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void onScam()
    {
        diver.SendMessage("onScam");
    }
    void slow()
    {
        diver.SendMessage("slow");
    }
    void takeDamage()
    {
        diver.SendMessage("takeDamage");
    }
    void setMoving(bool yesno)
    {
        
        diver.SendMessage("setMoving", yesno);
    }

    void setTarget(Vector3 target)
    {
        diver.SendMessage("setTarget", target);
    }
    void toggleCollider()
    {
        diver.SendMessage("toggleCollider");
    }
}
